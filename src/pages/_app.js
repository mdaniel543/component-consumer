import 'styles/general.css';
import 'styles/global.css';
import "components-react/dist/styles.css";
import ThemeCustomization from 'themes';

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />;
}
