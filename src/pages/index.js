import { useTema } from 'themes/tema';
import { Suspense } from 'react';
import { SnButton } from 'components-react';

const Home = () => {
  const themes = useTema();

  return (
    <Suspense
      fallback={
        <div className="w-full ">
          <p>Loading...</p>
        </div>
      }
    >
      <div className="w-full flex justify-center gap-5 p-6 items-center flex-col">
        HOLA MUNDO
        <SnButton onClick={() => console.log('click')}>
          Vengo del consumidor
        </SnButton>
      </div>
    </Suspense>
  );
};

export default Home;
