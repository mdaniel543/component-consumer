import Palette from './palette';
import Typography from './typography';
import CustomShadows from './shadows';
import componentsOverride from './overrides';
import config from 'config';
import { createTheme } from '@mui/material/styles';


export const useTema = () => {
  const { mode, fontFamily, presetColor, themeDirection } = config;

  const theme = Palette(mode, presetColor);

  const themeTypography = Typography(fontFamily);
  const themeCustomShadows = CustomShadows(theme);

  const themeOptions = {
    breakpoints: {
      values: {
        xs: 0,
        sm: 768,
        md: 1024,
        lg: 1266,
        xl: 1440
      }
    },
    direction: themeDirection,
    mixins: {
      toolbar: {
        minHeight: 60,
        paddingTop: 8,
        paddingBottom: 8
      }
    },
    palette: theme.palette,
    customShadows: themeCustomShadows,
    typography: themeTypography
  };

  let themes = createTheme(themeOptions);
  themes.components = componentsOverride(themes);

  return themes;
};
