export const drawerWidth = 260;

export const twitterColor = '#1DA1F2';
export const facebookColor = '#3b5998';
export const linkedInColor = '#0e76a8';

export const LAYOUT_CONST = {
  VERTICAL_LAYOUT: 'vertical',
  HORIZONTAL_LAYOUT: 'horizontal'
};

export const HORIZONTAL_MAX_ITEM = 6;
export const DEFAULT_PATH = '/';

// ==============================|| THEME CONFIG  ||============================== //

const config = {
  mode: 'light',
  drawerOpen: false,
  presetColor: 'theme1',
  themeDirection: 'ltr',
  fontFamily: `'Inter', sans-serif`,
  i18n: 'es',
  menuOrientation: 'vertical',
  miniDrawer: true,
};

export default config;
