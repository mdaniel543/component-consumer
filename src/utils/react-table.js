import PropTypes from 'prop-types';
import { useEffect, useMemo, useState } from 'react';
import Moment from 'moment';
// material-ui
import {
  FormControl,
  MenuItem,
  OutlinedInput,
  Select,
  Slider,
  Stack,
  TextField,
  Tooltip,
  InputLabel,
  IconButton,
  Box,
  Chip
} from '@mui/material';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

import { GET_TIPO } from 'graphql/querys/others';
import SelectAuto from 'containers/inputs/SelectAuto';

import { dispatch, useDispatch } from 'store';
import {
  addFiltro,
  removeFiltro,
  addFiltroBetween,
  addSubFiltroNit
} from 'store/reducers/clientes';
import {
  addFiltro as addFiltroCotizacion,
  addSubFiltroCotizacion,
  removeFiltro as removeFiltroCotizacion
} from 'store/reducers/cotizacion';

import {
  addFiltro as addFiltroVenta,
  addSubFiltroVenta,
  removeFiltro as removeFiltroVenta
} from 'store/reducers/venta';

// third-party
import { useAsyncDebounce } from 'react-table';
import { matchSorter } from 'match-sorter';
import { format } from 'date-fns';
import {
  addFiltroArticulo,
  removeFiltroArticulo,
  addSubFiltroArticulo
} from 'store/reducers/articulos';

// project import
import { SnInput } from 'components/SnComponents';
import { useIntl } from 'react-intl';
// assets
import { CloseOutlined, LineOutlined, SearchOutlined } from '@ant-design/icons';

export function GlobalFilter({ preGlobalFilteredRows, globalFilter, setGlobalFilter, title }) {
  const count = preGlobalFilteredRows.length;
  const [value, setValue] = useState(globalFilter);
  const onChange = useAsyncDebounce((value) => {
    setGlobalFilter(value || undefined);
  }, 200);
  const intl = useIntl();

  return (
    <Box
      sx={{
        width: '395px'
      }}
    >
      <SnInput
        id="globalFilter"
        color={'#9B59B6'}
        startAdornment={true}
        value={value || ''}
        setValue={(value) => {
          setValue(value);
          onChange(value);
        }}
        placeholder={intl.formatMessage({ id: 'Buscar en el detalle' })}
        size="small"
        top="0px"
        height={32}
      />
    </Box>
  );
}

GlobalFilter.propTypes = {
  preGlobalFilteredRows: PropTypes.array,
  globalFilter: PropTypes.string,
  setGlobalFilter: PropTypes.func
};

const subs = ['categoria', 'subcategoria', 'cliente', 'documento'];

export function DefaultColumnFilterDOC({ column: { filterValue, Header, setFilter } }) {
  return (
    <TextField
      fullWidth
      value={filterValue || ''}
      onChange={(e) => {
        setFilter(e.target.value || undefined);
      }}
      placeholder={Header}
      size="small"
    />
  );
}

DefaultColumnFilterDOC.propTypes = {
  column: PropTypes.object
};

export function DefaultColumnFilter({ column: { Header, id }, title }) {
  const dispatch = useDispatch();
  const [value, setValue] = useState();

  return (
    <TextField
      fullWidth
      value={value || ''}
      onChange={(e) => {
        setValue(e.target.value);
        if (e.target.value !== '') {
          const campo = id;
          //const valor = isNaN(e.target.value) ? e.target.value : Number(e.target.value);
          const valor = e.target.value;
          //const tipo = isNaN(e.target.value) ? 'contains' : 'eq';
          const tipo = 'contains';
          if (title === 'productos') {
            if (subs.includes(campo)) {
              dispatch(addSubFiltroArticulo({ campo, valor, tipo }));
            } else {
              dispatch(addFiltroArticulo({ campo, valor, tipo }));
            }
          } else if (title === 'cotizaciones') {
            if (subs.includes(campo)) {
              dispatch(addSubFiltroCotizacion({ campo, valor, tipo }));
            } else {
              dispatch(addFiltroCotizacion({ campo, valor, tipo }));
            }
          } else if (title === 'ventas') {
            if (subs.includes(campo)) {
              dispatch(addSubFiltroVenta({ campo, valor, tipo }));
            } else {
              dispatch(addFiltroVenta({ campo, valor, tipo }));
            }
          } else {
            if (campo === 'nit') {
              dispatch(addSubFiltroNit({ campo, valor, tipo }));
              return;
            }
            dispatch(addFiltro({ campo, valor, tipo }));
          }
        } else {
          if (title === 'productos') {
            dispatch(removeFiltroArticulo({ campo: id }));
          } else if (title === 'cotizaciones') {
            dispatch(removeFiltroCotizacion({ campo: id }));
          } else if (title === 'ventas') {
            dispatch(removeFiltroVenta({ campo: id }));
          } else {
            dispatch(removeFiltro({ campo: id }));
          }
        }
      }}
      placeholder={Header}
      size="small"
      type="search"
    />
  );
}

DefaultColumnFilter.propTypes = {
  column: PropTypes.object
};

export function NumberColumnFilter({ column: { Header, id }, title }) {
  const dispatch = useDispatch();
  const [value, setValue] = useState();

  return (
    <TextField
      fullWidth
      value={value || ''}
      onChange={(e) => {
        setValue(e.target.value);
        if (e.target.value !== '') {
          const campo = id;
          const valor = Number(e.target.value);
          const tipo = 'eq';
          if (title === 'productos') {
            dispatch(addFiltroArticulo({ campo, valor, tipo }));
          } else if (title === 'cotizaciones') {
            dispatch(addFiltroCotizacion({ campo, valor, tipo }));
          } else if (title === 'ventas') {
            dispatch(addFiltroVenta({ campo, valor, tipo }));
          } else {
            dispatch(addFiltro({ campo, valor, tipo }));
          }
        } else {
          if (title === 'productos') {
            dispatch(removeFiltroArticulo({ campo: id }));
          } else if (title === 'cotizaciones') {
            dispatch(removeFiltroCotizacion({ campo: id }));
          } else if (title === 'ventas') {
            dispatch(removeFiltroVenta({ campo: id }));
          } else {
            dispatch(removeFiltro({ campo: id }));
          }
        }
      }}
      placeholder={Header}
      size="small"
      type="search"
    />
  );
}

NumberColumnFilter.propTypes = {
  column: PropTypes.object
};

export function StatusFilter({ column: { Header, id } }) {
  const dispatch = useDispatch();
  const [value, setValue] = useState();
  return (
    <FormControl fullWidth size="small">
      <InputLabel
        id="demo-simple-select-label"
        sx={{
          color: 'text.disabled'
        }}
      >
        {Header}
      </InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={value}
        onChange={(e) => {
          setValue(e.target.value);
          if (e.target.value !== '') {
            const campo = id;
            //const valor = isNaN(e.target.value) ? e.target.value : Number(e.target.value);
            const valor = e.target.value;
            //const tipo = isNaN(e.target.value) ? 'contains' : 'eq';
            const tipo = 'eq';
            dispatch(addFiltro({ campo, valor, tipo }));
          } else {
            dispatch(removeFiltro({ campo: id }));
          }
        }}
      >
        <MenuItem value={true}>Habilitado</MenuItem>
        <MenuItem value={false}>Deshabilitado</MenuItem>
      </Select>
    </FormControl>
  );
}

export const TipoClienteFilter = ({ column: { Header, id } }) => {
  return (
    <div className="-mt-2">
      <SelectAuto
        Query={GET_TIPO}
        code={'tipoCliente'}
        id={'idTipoCliente'}
        object={'tipoCliente'}
        updateObjetos={(data) => {
          if (data.tipoCliente === null) {
            dispatch(removeFiltro({ campo: 'idTipoCliente' }));
            return;
          }
          dispatch(
            addFiltro({ campo: 'idTipoCliente', valor: data?.tipoCliente?.idCualidad, tipo: 'eq' })
          );
        }}
        update={() => {}}
        showID={false}
        showDot={false}
        required={false}
        showIconStart={false}
      />
    </div>
  );
};

export const StatusCellDocument = ({ value }) => {
  switch (value) {
    case 'VOID':
      return <Chip color="error" label="Anulado" size="small" variant="light" />;
    case 'VALID':
      return <Chip color="success" label="Aplicado" size="small" variant="light" />;
    case 'ANNULMENT':
      return <Chip color="info" label="Guardado" size="small" variant="light" />;
    case 'HOLD':
    default:
      return <Chip color="warning" label="Pendiente" size="small" variant="light" />;
  }
};

export function StatusFilterNumber({ column: { Header, id } }) {
  const dispatch = useDispatch();
  const [value, setValue] = useState();
  return (
    <FormControl fullWidth size="small">
      <InputLabel
        id="demo-simple-select-label"
        sx={{
          color: 'text.disabled'
        }}
      >
        {Header}
      </InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={value}
        onChange={(e) => {
          setValue(e.target.value);
          if (e.target.value !== '') {
            const campo = id;
            //const valor = isNaN(e.target.value) ? e.target.value : Number(e.target.value);
            const valor = e.target.value;
            //const tipo = isNaN(e.target.value) ? 'contains' : 'eq';
            const tipo = 'eq';
            dispatch(addFiltroCotizacion({ campo, valor, tipo }));
          } else {
            dispatch(removeFiltroCotizacion({ campo: id }));
          }
        }}
      >
        <MenuItem value={'VOID'}>Anulado</MenuItem>
        <MenuItem value={'VALID'}>Aplicado</MenuItem>
        <MenuItem value={'ANNULMENT'}>Guardado</MenuItem>
        <MenuItem value={'HOLD'}>Pendiente</MenuItem>
      </Select>
    </FormControl>
  );
}

export function DateColumnFilter({ column: { Header, id } }) {
  const [filterValue, setFilter] = useState(null);
  const dispatch = useDispatch();
  return (
    <FormControl sx={{ width: '100%' }}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <DatePicker
          inputFormat="dd/MM/yyyy"
          value={filterValue || null}
          onChange={(newValue) => {
            setFilter(newValue || undefined);
            if (newValue !== null) {
              const campo = id;
              const valor = Moment(newValue).format('YYYY/MM/DD');
              const tipo = 'eq';
              dispatch(addFiltroCotizacion({ campo, valor, tipo }));
            } else {
              dispatch(removeFiltroCotizacion({ campo: id }));
            }
          }}
          renderInput={(params) => (
            <TextField name={Header} {...params} placeholder={`Select ${Header}`} />
          )}
        />
      </LocalizationProvider>
    </FormControl>
  );
}

DateColumnFilter.propTypes = {
  column: PropTypes.object
};

export function SelectColumnFilter({ column: { filterValue, setFilter, preFilteredRows, id } }) {
  const options = useMemo(() => {
    const options = new Set();
    preFilteredRows.forEach((row) => {
      options.add(row.values[id]);
    });
    return [...options.values()];
  }, [id, preFilteredRows]);

  return (
    <Select
      value={filterValue}
      onChange={(e) => {
        setFilter(e.target.value || undefined);
      }}
      displayEmpty
      size="small"
    >
      <MenuItem value="">All</MenuItem>
      {options.map((option, i) => (
        <MenuItem key={i} value={option}>
          {option}
        </MenuItem>
      ))}
    </Select>
  );
}

SelectColumnFilter.propTypes = {
  column: PropTypes.object
};

export function SliderColumnFilter({ column: { id } }) {
  /*/const [min, max] = useMemo(() => {
    let min = preFilteredRows.length ? preFilteredRows[0].values[id] : 0;
    let max = preFilteredRows.length ? preFilteredRows[0].values[id] : 0;
    preFilteredRows.forEach((row) => {
      min = Math.min(row.values[id], min);
      max = Math.max(row.values[id], max);
    });
    return [min, max];
  }, [id, preFilteredRows]);*/

  const dispatch = useDispatch();
  const [value, setValue] = useState(0);

  return (
    <Stack direction="row" alignItems="center" spacing={1} sx={{ pl: 1, minWidth: 120 }}>
      <Slider
        value={value || 0}
        /*min={min}
        max={max}*/
        step={1}
        onChange={(e, newValue) => {
          setValue(e.target.value);
          if (e.target.value != 0) {
            const campo = id;
            //const valor = isNaN(e.target.value) ? e.target.value : Number(e.target.value);
            const valor = Number(e.target.value);
            //const tipo = isNaN(e.target.value) ? 'contains' : 'eq';
            const tipo = 'eq';
            dispatch(addFiltro({ campo, valor, tipo }));
          } else {
            dispatch(removeFiltro({ campo: id }));
          }
        }}
        valueLabelDisplay="auto"
        aria-labelledby="non-linear-slider"
      />
      <Tooltip title="Reset">
        <IconButton
          size="small"
          color="error"
          onClick={() => {
            setValue(0);
            dispatch(removeFiltro({ campo: id }));
          }}
        >
          <CloseOutlined />
        </IconButton>
      </Tooltip>
    </Stack>
  );
}

SliderColumnFilter.propTypes = {
  column: PropTypes.object
};

export function NumberRangeColumnFilter({
  column: { filterValue = [], preFilteredRows, setFilter, id }
}) {
  const dispatch = useDispatch();
  const [value, setValue] = useState('');
  const [value2, setValue2] = useState('');

  useEffect(() => {
    if ((value != 0 && value2 != 0) || (value != '' && value2 != '')) {
      const campo = id;
      const valor1 = Number(value);
      const valor2 = Number(value2);

      dispatch(addFiltroBetween({ campo, valor1, valor2 }));
    } else {
      dispatch(removeFiltro({ campo: id }));
    }
  }, [value, value2]);

  return (
    <Stack direction="row" alignItems="center" spacing={1} sx={{ minWidth: 168, maxWidth: 250 }}>
      <TextField
        fullWidth
        value={value}
        type="number"
        onChange={(e) => {
          setValue(e.target.value);
        }}
        placeholder={`Min`}
        size="small"
      />
      <TextField
        fullWidth
        value={value2}
        type="number"
        onChange={(e) => {
          setValue2(e.target.value);
        }}
        placeholder={`Max`}
        size="small"
        style={{ marginLeft: 2, marginRight: 2 }}
      />
    </Stack>
  );
}

NumberRangeColumnFilter.propTypes = {
  column: PropTypes.object
};

function fuzzyTextFilterFn(rows, id, filterValue) {
  return matchSorter(rows, filterValue, { keys: [(row) => row.values[id]] });
}

fuzzyTextFilterFn.autoRemove = (val) => !val;

export const renderFilterTypes = () => ({
  fuzzyText: fuzzyTextFilterFn,
  text: (rows, id, filterValue) => {
    rows.filter((row) => {
      const rowValue = row.values[id];
      return rowValue !== undefined
        ? String(rowValue).toLowerCase().startsWith(String(filterValue).toLowerCase())
        : true;
    });
  }
});

export function filterGreaterThan(rows, id, filterValue) {
  return rows.filter((row) => {
    const rowValue = row.values[id];
    return rowValue >= filterValue;
  });
}

filterGreaterThan.autoRemove = (val) => typeof val !== 'number';

export function useControlledState(state) {
  return useMemo(() => {
    if (state.groupBy.length) {
      return {
        ...state,
        hiddenColumns: [...state.hiddenColumns, ...state.groupBy].filter(
          (d, i, all) => all.indexOf(d) === i
        )
      };
    }
    return state;
  }, [state]);
}

export function roundedMedian(leafValues) {
  let min = leafValues[0] || 0;
  let max = leafValues[0] || 0;

  leafValues.forEach((value) => {
    min = Math.min(min, value);
    max = Math.max(max, value);
  });

  return Math.round((min + max) / 2);
}
