/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: {
          100: '#d6f6ff',
          200: '#d6f5ff',
          300: '#ade6ff',
          400: '#2BB6F6',
          500: '#2BB6F6',
          600: '#2BB6F6',
          700: '#20A3DF',
          800: '#2BB6F6',
          900: '#20A3DF',
          A50: '#2BB6F6',
          A100: '#20A3DF',
          A200: '#2BB6F6',
          A400: '#20A3DF',
          A700: '#2BB6F6'
        },
        error: {
          100: '#FFE7D3',
          200: '#FF805D',
          300: '#FF4528',
          400: '#DB271D',
          500: '#930C1A'
        },
        warning: {
          100: '#FFF6D0',
          200: '#FFCF4E',
          300: '#FFB814',
          400: '#ED9E28',
          500: '#935B06'
        },
        info: {
          100: '#DCF0FF',
          200: '#7EB9FF',
          300: '#549BFF',
          400: '#006CBE',
          500: '#1A3D93',
        },
        success: {
          100: '#EAFCD4',
          200: '#8AE65B',
          300: '#58D62A',
          400: '#3DB81E',
          500: '#137C0D'
        },
        auxiliar: {
          100: '#FFCC00'
        },
        background: {
          100: '#F5F5F5',
          200: '#E5E5E5',
          300: '#D6D6D6',
          400: '#BCBCBC',
          500: '#BCBCBC',
          600: '#666666',
          700: '#4D4D4D',
          800: '#333333',
          900: '#1A1A1A'
        }
      }
    }
  },
  plugins: []
};
